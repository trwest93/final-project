class Amortization < ActiveRecord::Base
  belongs_to :sale

  def combine_name_price
    "#{quote.quote_name} #{quote.total_price}"
  end

  def calculate_quote
    i = interest/100
    t = quote.total_price
    u = terms * 12
    m = ((i * t * (1+i)**t)/(((1+i)**u)-1))
  end
end
