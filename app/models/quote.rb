class Quote < ActiveRecord::Base
  belongs_to :car
  belongs_to :customer
  belongs_to :employee
  has_many :sales
end
