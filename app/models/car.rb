class Car < ActiveRecord::Base
  belongs_to :make
  belongs_to :color
  belongs_to :year
  has_many :quotes
end
