class Sale < ActiveRecord::Base
  belongs_to :quote
  has_many :overalls
  has_many :amortizations
end
