class Employee < ActiveRecord::Base
  has_many :quotes
  belongs_to :emp_type
  def full_name
    "#{fname} #{lname}"
  end
end
