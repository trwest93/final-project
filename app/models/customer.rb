class Customer < ActiveRecord::Base
  has_many :quotes
  def full_name
    "#{fname} #{lname}"
  end
end
