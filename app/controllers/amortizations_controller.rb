class AmortizationsController < ApplicationController
  before_action :set_amortization, only: [:show, :edit, :update, :destroy]

  # GET /amortizations
  # GET /amortizations.json
  def index
    @amortizations = Amortization.all
  end

  # GET /amortizations/1
  # GET /amortizations/1.json
  def show
  end

  # GET /amortizations/new
  def new
    @amortization = Amortization.new
  end

  # GET /amortizations/1/edit
  def edit
  end

  # POST /amortizations
  # POST /amortizations.json
  def create
    @amortization = Amortization.new(amortization_params)

    respond_to do |format|
      if @amortization.save
        format.html { redirect_to @amortization, notice: 'Amortization was successfully created.' }
        format.json { render :show, status: :created, location: @amortization }
      else
        format.html { render :new }
        format.json { render json: @amortization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /amortizations/1
  # PATCH/PUT /amortizations/1.json
  def update
    respond_to do |format|
      if @amortization.update(amortization_params)
        format.html { redirect_to @amortization, notice: 'Amortization was successfully updated.' }
        format.json { render :show, status: :ok, location: @amortization }
      else
        format.html { render :edit }
        format.json { render json: @amortization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /amortizations/1
  # DELETE /amortizations/1.json
  def destroy
    @amortization.destroy
    respond_to do |format|
      format.html { redirect_to amortizations_url, notice: 'Amortization was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_amortization
      @amortization = Amortization.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def amortization_params
      params.require(:amortization).permit(:term_years, :interest, :month_pay, :sale_id)
    end
end
