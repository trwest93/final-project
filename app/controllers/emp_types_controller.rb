class EmpTypesController < ApplicationController
  before_action :set_emp_type, only: [:show, :edit, :update, :destroy]

  # GET /emp_types
  # GET /emp_types.json
  def index
    @emp_types = EmpType.all
  end

  # GET /emp_types/1
  # GET /emp_types/1.json
  def show
  end

  # GET /emp_types/new
  def new
    @emp_type = EmpType.new
  end

  # GET /emp_types/1/edit
  def edit
  end

  # POST /emp_types
  # POST /emp_types.json
  def create
    @emp_type = EmpType.new(emp_type_params)

    respond_to do |format|
      if @emp_type.save
        format.html { redirect_to @emp_type, notice: 'Emp type was successfully created.' }
        format.json { render :show, status: :created, location: @emp_type }
      else
        format.html { render :new }
        format.json { render json: @emp_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /emp_types/1
  # PATCH/PUT /emp_types/1.json
  def update
    respond_to do |format|
      if @emp_type.update(emp_type_params)
        format.html { redirect_to @emp_type, notice: 'Emp type was successfully updated.' }
        format.json { render :show, status: :ok, location: @emp_type }
      else
        format.html { render :edit }
        format.json { render json: @emp_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /emp_types/1
  # DELETE /emp_types/1.json
  def destroy
    @emp_type.destroy
    respond_to do |format|
      format.html { redirect_to emp_types_url, notice: 'Emp type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_emp_type
      @emp_type = EmpType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def emp_type_params
      params.require(:emp_type).permit(:emp_type)
    end
end
