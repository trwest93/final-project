json.array!(@amortizations) do |amortization|
  json.extract! amortization, :id, :term_years, :interest, :month_pay, :sale_id
  json.url amortization_url(amortization, format: :json)
end
