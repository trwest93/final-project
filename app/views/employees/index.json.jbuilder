json.array!(@employees) do |employee|
  json.extract! employee, :id, :fname, :lname, :phone, :email, :emp_type
  json.url employee_url(employee, format: :json)
end
