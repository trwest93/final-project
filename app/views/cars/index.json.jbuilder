json.array!(@cars) do |car|
  json.extract! car, :id, :vin_num, :car_price, :make_id, :color_id, :year_id
  json.url car_url(car, format: :json)
end
