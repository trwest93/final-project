json.array!(@quotes) do |quote|
  json.extract! quote, :id, :quote_name, :mark_price, :tax, :total_price, :date_started, :sold, :car_id
  json.url quote_url(quote, format: :json)
end
