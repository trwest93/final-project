json.array!(@sales) do |sale|
  json.extract! sale, :id, :sale_total, :quote_id, :customer_id, :employee_id
  json.url sale_url(sale, format: :json)
end
