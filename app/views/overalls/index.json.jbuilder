json.array!(@overalls) do |overall|
  json.extract! overall, :id, :total_gross, :net_profit, :total_tax, :sale_id
  json.url overall_url(overall, format: :json)
end
