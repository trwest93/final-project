json.array!(@emp_types) do |emp_type|
  json.extract! emp_type, :id, :emp_type
  json.url emp_type_url(emp_type, format: :json)
end
