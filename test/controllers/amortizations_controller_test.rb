require 'test_helper'

class AmortizationsControllerTest < ActionController::TestCase
  setup do
    @amortization = amortizations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:amortizations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create amortization" do
    assert_difference('Amortization.count') do
      post :create, amortization: { interest: @amortization.interest, month_pay: @amortization.month_pay, sale_id: @amortization.sale_id, term_years: @amortization.term_years }
    end

    assert_redirected_to amortization_path(assigns(:amortization))
  end

  test "should show amortization" do
    get :show, id: @amortization
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @amortization
    assert_response :success
  end

  test "should update amortization" do
    patch :update, id: @amortization, amortization: { interest: @amortization.interest, month_pay: @amortization.month_pay, sale_id: @amortization.sale_id, term_years: @amortization.term_years }
    assert_redirected_to amortization_path(assigns(:amortization))
  end

  test "should destroy amortization" do
    assert_difference('Amortization.count', -1) do
      delete :destroy, id: @amortization
    end

    assert_redirected_to amortizations_path
  end
end
