require 'test_helper'

class EmpTypesControllerTest < ActionController::TestCase
  setup do
    @emp_type = emp_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:emp_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create emp_type" do
    assert_difference('EmpType.count') do
      post :create, emp_type: { emp_type: @emp_type.emp_type }
    end

    assert_redirected_to emp_type_path(assigns(:emp_type))
  end

  test "should show emp_type" do
    get :show, id: @emp_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @emp_type
    assert_response :success
  end

  test "should update emp_type" do
    patch :update, id: @emp_type, emp_type: { emp_type: @emp_type.emp_type }
    assert_redirected_to emp_type_path(assigns(:emp_type))
  end

  test "should destroy emp_type" do
    assert_difference('EmpType.count', -1) do
      delete :destroy, id: @emp_type
    end

    assert_redirected_to emp_types_path
  end
end
