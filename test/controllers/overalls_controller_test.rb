require 'test_helper'

class OverallsControllerTest < ActionController::TestCase
  setup do
    @overall = overalls(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:overalls)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create overall" do
    assert_difference('Overall.count') do
      post :create, overall: { net_profit: @overall.net_profit, sale_id: @overall.sale_id, total_gross: @overall.total_gross, total_tax: @overall.total_tax }
    end

    assert_redirected_to overall_path(assigns(:overall))
  end

  test "should show overall" do
    get :show, id: @overall
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @overall
    assert_response :success
  end

  test "should update overall" do
    patch :update, id: @overall, overall: { net_profit: @overall.net_profit, sale_id: @overall.sale_id, total_gross: @overall.total_gross, total_tax: @overall.total_tax }
    assert_redirected_to overall_path(assigns(:overall))
  end

  test "should destroy overall" do
    assert_difference('Overall.count', -1) do
      delete :destroy, id: @overall
    end

    assert_redirected_to overalls_path
  end
end
