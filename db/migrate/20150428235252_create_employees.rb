class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :fname
      t.string :lname
      t.integer :phone
      t.string :email
      t.integer :emp_type

      t.timestamps null: false
    end
  end
end
