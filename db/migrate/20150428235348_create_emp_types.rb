class CreateEmpTypes < ActiveRecord::Migration
  def change
    create_table :emp_types do |t|
      t.string :emp_type

      t.timestamps null: false
    end
  end
end
