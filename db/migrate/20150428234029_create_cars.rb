class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :vin_num
      t.decimal :car_price
      t.integer :make_id
      t.integer :color_id
      t.integer :year_id

      t.timestamps null: false
    end
  end
end
