class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.string :quote_name
      t.decimal :mark_price
      t.integer :tax
      t.integer :total_price
      t.date :date_started
      t.boolean :sold
      t.integer :car_id

      t.timestamps null: false
    end
  end
end
