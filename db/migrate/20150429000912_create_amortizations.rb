class CreateAmortizations < ActiveRecord::Migration
  def change
    create_table :amortizations do |t|
      t.integer :term_years
      t.float :interest
      t.integer :month_pay
      t.integer :sale_id

      t.timestamps null: false
    end
  end
end
