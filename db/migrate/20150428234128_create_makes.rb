class CreateMakes < ActiveRecord::Migration
  def change
    create_table :makes do |t|
      t.string :make_name

      t.timestamps null: false
    end
  end
end
