class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.integer :sale_total
      t.integer :quote_id
      t.integer :customer_id
      t.integer :employee_id

      t.timestamps null: false
    end
  end
end
