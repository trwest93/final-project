class CreateOveralls < ActiveRecord::Migration
  def change
    create_table :overalls do |t|
      t.decimal :total_gross
      t.decimal :net_profit
      t.decimal :total_tax
      t.integer :sale_id

      t.timestamps null: false
    end
  end
end
