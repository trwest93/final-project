# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
car_list = [
    [ "AB474859687F7474R33", 20000.33, "Toyota Corolla", 2002, "Red"],
    [ "BD835D7Y47854FH47", 2344.22, "Toyota Camry", 1999, "Green"],
    [ "9H4H454749KS7447E", 13000.23, "Ford Fusion", 1995, "White"],
    [ "933HHD8WI32IDYFY", 12300.12, "Cadilliac Escalade", 1997, "Red"],
    [ "C3259B5GD46DH57D6", 19000.32, "Nissan Pathfinder", 2005, "Blue"],
    [ "J36DGH46DGER45DR4", 20000.22, "Toyota Corolla", 2007, "Black"],
    [ "2DB46FF4V7DPG654", 21000.56, "Toyota Corolla", 2002, "Black"],
    [ "FD1248745FDE44145", 18000.74, "Toyota Corolla", 1987, "Yellow"],
    [ "CR74HC56FGD44404", 20345.14, "Toyota Corolla", 1999, "White"],
    [ "D124574SFT41475D3", 20000, "Toyota Corolla", 2000, "Blue"],
    [ "BHDGGUD4736454F47", 20000, "Toyota Corolla", 2001, "White"],
    [ "21C548745874521", 20000, "Toyota Corolla", 1995, "Black"],
    [ "Y47HCDDUEYRH4H", 20000, "Toyota Corolla", 2011, "Red"],

]
car_list.each do |vin_num, car_price, make_name, year, color|
  Car.create( vin_num: vin_num, car_price: car_price, make_name: make_name, year: year, color: color )
end